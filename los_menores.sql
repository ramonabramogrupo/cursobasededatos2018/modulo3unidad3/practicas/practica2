/* NUMERO 9 PRACTICA 2 U3 M3 */

 --  menores de edad
  CREATE OR REPLACE VIEW menores_edad  AS
    SELECT 
        nombre,
        edad,
        apellidos 
  FROM personas WHERE edad<18;

  --  tablas de menores y mayores

  -- opcion 1
  CREATE OR REPLACE TABLE menores(
  nombre varchar(50),
    edad int,
    apellidos varchar(50)
  ) AS SELECT * FROM menores_edad;

  -- otra opcion
  CREATE OR REPLACE TABLE menores 
  AS SELECT * FROM menores_edad;

  -- no menores otra opcion
  CREATE OR REPLACE TABLE no_menores(
  nombre varchar(50),
    edad int,
    apellidos varchar(50)
  );

  --  insercion de datos
  
  INSERT INTO no_menores  
    SELECT personas.nombre,personas.edad,personas.apellidos 
    FROM personas LEFT JOIN menores_edad 
    USING(nombre) WHERE menores_edad.nombre IS NULL;


-- version ramon
CREATE OR REPLACE VIEW v1 AS 
  SELECT p.nombre, p.apellidos, p.edad FROM personas p 
    WHERE p.edad<18;

DROP TABLE IF EXISTS menores;
CREATE TABLE menores (
  SELECT * FROM v1
  );

DROP TABLE IF EXISTS no_menores;
CREATE TABLE no_menores(
    SELECT p.nombre, p.apellidos, p.edad FROM  personas p 
    WHERE p.edad>=18
  );
