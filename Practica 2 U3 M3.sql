/*  PRACTICA 2 U3 M3 */

DROP DATABASE IF EXISTS practica2u3m3;
CREATE DATABASE if NOT EXISTS practica2u3m3;
USE practica2u3m3;


/* 1-
-- procedimiento que crea una tabla
-- Explicar el funcionamiento del siguiente procedimiento 
  y despu�s ejecutarle
*/

DELIMITER // 

DROP PROCEDURE IF EXISTS f1// -- borra el procedimiento si existe
CREATE PROCEDURE f1() -- crea el procedimiento f1
  BEGIN -- comienza el procedimiento
    CREATE TABLE tabla1( -- instruccion de creacion de tabla
      nombre varchar(10) DEFAULT NULL,
      edad int(9) DEFAULT 0,
      Fecha_nacimiento date DEFAULT null
      );
  END// -- fin del procedimiento
DELIMITER ; -- vuelve a poner ; como caracter de fin de instruccion

CALL f1();-- llamando al procedimiento

-- 2-
-- Modificar el procedimiento anterior para crear una tabla 
-- con los siguientes campos:

DELIMITER //
DROP PROCEDURE IF EXISTS crear_tabla//
CREATE PROCEDURE crear_tabla ()
BEGIN

  CREATE TABLE IF NOT EXISTS personas (
    dni decimal(8),
    nombre varchar(30),
    edad integer,
    apellidos varchar(50),
    fecha_nacimiento date
  );
END//
DELIMITER ;

-- version ramon
DELIMITER //
DROP PROCEDURE IF EXISTS crear_tabla//
CREATE PROCEDURE crear_tabla()
  BEGIN
    CREATE OR REPLACE TABLE personas(
      dni decimal(8),
      nombre varchar(30),
      edad int,
      apellidos varchar(50),
      fecha_nacimiento date
      );
END//
DELIMITER ;

CALL crear_tabla();

-- 3-
-- Explicar el funcionamiento del siguiente procedimiento 
-- y despu�s ejecutarle
-- procedimiento que evalua si una tabla esta vacia o no

-- cambio el caracter de fin de instruccion
DELIMITER // 

DROP PROCEDURE IF EXISTS f2// -- borra el procedimiento si existe
CREATE PROCEDURE f2() -- crea el procedimiento f2
  BEGIN -- comienzo del procedimiento
    DECLARE v1 int; -- crea una variable de tipo entero

    SET v1=(SELECT COUNT(*) FROM  tabla1); -- guarda en v1 el valor de la consulta de totales
    /* otra opcion */
    SELECT COUNT(*) INTO v1 FROM  tabla1;

    IF (v1=0) THEN -- si v2 es igual a cero entonces
      SELECT 'la tabla esta vacia' salida; -- muestra el texto 'la tabla esta vacia' con la cabecera salida
    ELSE -- sino
      SELECT 'la tabla no esta vacia' salida; -- muestra el texto 'la tabla no esta vacia' con la cabecera salida
    END IF; -- fin del if

END// -- fin de procedimiento
-- vuelve a poner ; como caracter de fin de instruccion
DELIMITER ; 

CALL f2();

-- 4
-- Modificar el procedimiento anterior para que si le pasamos un 0 te evalua la tabla denominada tabla1, si es un 1 eval�a la tabla denominada personas. Si le pasa cualquier otro valor 
-- entonces debe indicar valor incorrecto.

DELIMITER //
CREATE OR REPLACE PROCEDURE ej4(IN n int)
  COMMENT 'Este procedimiento devolver� una tabla u otra dependiendo del valor de entrada introducido'
  BEGIN
  
  IF (n=0) THEN 
    SELECT * FROM tabla1;
  ELSEIF (n=1) THEN 
    SELECT * FROM personas;
  ELSE 
    SELECT 'valor incorrecto';
  END IF;
  END //
DELIMITER ;

-- con case

DELIMITER //
CREATE OR REPLACE PROCEDURE ej4Case(IN n int)
  COMMENT 'Este procedimiento devolver� una tabla u otra dependiendo del valor de entrada introducido'
  BEGIN
  CASE n
    WHEN 0 THEN 
      SELECT * FROM tabla1;
    WHEN 1 then
      SELECT * FROM personas;
    ELSE 
      SELECT 'valor incorrecto';
  END CASE;
  END //
DELIMITER ;


CALL ej4(0);

-- version ramon
DELIMITER //
DROP PROCEDURE if EXISTS  f2//
CREATE PROCEDURE f2(arg1 int)
  BEGIN
    DECLARE v1 int;
    DECLARE t varchar(10);
    
    CASE 
    WHEN (arg1=0) THEN
      SET v1=(SELECT COUNT(*) FROM tabla1);
      SET t='tabla1';
      -- SELECT * FROM tabla1;
    WHEN (arg1=1) THEN
      SET v1=(SELECT COUNT(*) FROM personas);
      SET t='personas';
    ELSE
      set v1=-1;
    END CASE;

    IF (v1=0) THEN
        SELECT CONCAT('la tabla ', t,' esta vacia') salida;
    ELSEIF (v1=-1) THEN
        SELECT 'valor incorrecto' salida;
    ELSE
        SELECT CONCAT('la tabla ', t, ' no esta vacia' ) salida;
    end IF;
  END//
DELIMITER ;

CALL f2(1);

-- 5-

/* procedimiento que evalua si la tabla tabla1 tiene datos y si no los tiene inserta uno, si tiene entre 10 y 100, 
  los muestra y si tiene mas de 100 muestra los 100 primeros */
DELIMITER // -- cambia el caracter de fin de instruccion

DROP PROCEDURE IF EXISTS f3// -- borra el procedimiento si ya existe
CREATE PROCEDURE f3() -- crea el procedimiento f3
  BEGIN -- comienza el procedimiento
    declare v1 int; -- crea una variable v1 de tipo entero

    SET v1=(SELECT COUNT(*) from tabla1); -- guarda en v1 el valor de la consulta de totales
    IF (v1=0) THEN -- evalua si v1 vale 0, entonces
      SELECT 'la tabla esta vacia' salida; -- muestra el texto 'la tabla esta vacia'
      INSERT INTO tabla1 VALUES (DEFAULT, DEFAULT, DEFAULT); -- insert en la tabla tabla1 un registro con valores por defecto
    ELSEIF (v1 BETWEEN 10 AND 100) THEN -- evalua si v1 esta entre los valores 10 y 100, entonces
      SELECT * FROM tabla1 t; -- muestra todos los campos de tabla tabla1
    ELSE -- sino
      SELECT * FROM tabla1 LIMIT 1,100; -- muestra los 100 primeros registros de la tabla tabla1
    END if; -- fin del if

  END// -- fin del procedimiento
DELIMITER ; -- vuelve a poner ; como caracter de fin de instruccion

CALL f3();

-- 6
-- convertir en una funcion
-- error 
-- una funcion no puede realizar una instruccion que necesite commit 
-- select, update, delete, create, drop
-- puedo trabajar con tablas temporales
-- si podeis realizar un select si almacenais el resultado en una 
-- variable

/*
DELIMITER //
DROP FUNCTION IF EXISTS f3//
CREATE FUNCTION f3()
  RETURNS varchar(15)
  begin
    DECLARE v1 int;
    SET v1=(SELECT COUNT(*) FROM tabla1 t);
    IF(v1=0) THEN
      RETURN 'la tabla esta vacia';
      INSERT INTO tabla1 VALUES (DEFAULT, DEFAULT, DEFAULT);
    ELSEIF(v1 BETWEEN 10 AND 100) THEN
      RETURN select COUNT(*) FROM tabla1;
  END//
*/

-- 7
-- Realizar un procedimiento, denominado crear_claves, que genere las siguientes claves y restricciones en la tabla personas:
-- Clave principal : dni
-- Clave �nica: nombre y apellidos
-- Indexado con duplicados: fecha_nacimiento.

DELIMITER //
CREATE OR REPLACE PROCEDURE crear_claves()
  COMMENT 'Este procedimiento crea los constraints clave principal en dni,
           clave �nica en nombre y apellidos y el campo fecha nacimiento lo mete como indexado con duplicados'
  BEGIN
  ALTER TABLE personas ADD CONSTRAINT PRIMARYKEYDNI PRIMARY KEY(dni);
  ALTER TABLE personas ADD CONSTRAINT uniquekeynombre UNIQUE KEY(nombre,apellidos);
  ALTER TABLE personas ADD INDEX (fecha_nacimiento);
  END //
DELIMITER ;

-- version ramon

DELIMITER //
DROP PROCEDURE IF EXISTS crear_claves//
CREATE PROCEDURE crear_claves()
  begin
    ALTER TABLE personas ADD PRIMARY KEY (dni);
    SELECT 'a�adida clave primaria' PK;
    ALTER TABLE personas ADD UNIQUE (nombre, apellidos);
    SELECT 'a�adidos campos unicos' UK;
    ALTER TABLE personas ADD INDEX (fecha_nacimiento);
    SELECT 'a�adido indexado sin duplicados' IK;
END//
DELIMITER ;


CALL crear_claves();

-- 8

DELIMITER//
DROP PROCEDURE IF EXISTS funcionedad_p2//
CREATE PROCEDURE funcionedad_p2()
  COMMENT 'Crear un procedimiento que actualice todos los campos edad calcul�ndolos en funci�n de la fecha de nacimiento.'
  BEGIN
  
  UPDATE personas t
    SET t.edad=TIMESTAMPDIFF(year,t.fecha_nacimiento,CURDATE());

  END //
DELIMITER;

-- version ramon
DELIMITER //
DROP PROCEDURE IF EXISTS p9//
CREATE PROCEDURE p9()
  begin
    UPDATE personas p set edad=(year(NOW())-YEAR(p.fecha_nacimiento));
    SELECT 'campo edad actualizado' UP;
END//
DELIMITER ;

CALL p9();

-- 9

-- archivo los_menores.sql

-- 10
-- Crear una vista denominada nombres con los nombres y apellidos de todas las personas. Esta vista debe tener un solo campo denomina nombre_completo con el siguiente formato 
-- apellidos, nombre.


CREATE OR REPLACE VIEW nombres AS
  SELECT CONCAT(nombre,' ',apellidos) AS nombre_completo 
  FROM personas;

-- 11 
-- Crear un procedimiento el cual te rellene una tabla 
-- denominada alfabeto con todas las letras de la �a� a la �z�. 
-- Crear otro procedimiento que te rellene una tabla denominada n�meros con todos los n�meros del 1 al 1000. Estos procedimientos llamarlos 
-- crear_alfabeto y crear_numero.

--  letras

DELIMITER //
CREATE OR REPLACE PROCEDURE crear_alfabeto()
BEGIN
DECLARE contador int DEFAULT ASCII('a'); -- 97
DECLARE final int DEFAULT ASCII('z'); -- 122  

  CREATE OR REPLACE TABLE letras(
  letra char(3)  
  );

WHILE (contador<=final) DO
  INSERT INTO letras VALUE(char(contador));
  -- para colocar la � detras de la n
  IF(contador=ASCII('n')) THEN
    INSERT INTO letras VALUE('�');
  END IF;
  SET contador=contador+1;
END WHILE;
END//
DELIMITER ;

CALL crear_alfabeto();
SELECT * FROM letras;

-- version ramon
DELIMITER //
DROP PROCEDURE IF EXISTS crear_alfabeto//
CREATE PROCEDURE crear_alfabeto()
  BEGIN
    DECLARE l int DEFAULT ASCII('a');
    DROP TABLE IF EXISTS alfabeto;
    CREATE TABLE alfabeto(
      letra char(1)
      );
    REPEAT
      INSERT INTO alfabeto VALUES(CHAR(l));
      set l=l+1;
    UNTIL (l>ASCII('z'))
    END REPEAT;
END//
DELIMITER ;

CALL crear_alfabeto();


DELIMITER //
DROP PROCEDURE IF EXISTS crear_numero//
CREATE PROCEDURE crear_numero()
  BEGIN
    DECLARE n int DEFAULT 1;
    DROP TABLE IF EXISTS numeros;
    CREATE TABLE numeros(
      numero int
      );
    REPEAT
      INSERT INTO numeros VALUES(n);
      set n=n+1;
    UNTIL (n>1000)
    END REPEAT;
END//
DELIMITER ;

CALL crear_numero();

